package com.gohoc.library.util;

import android.util.Log;

/**
 * Created by keke on 2016.1.13.
 */
public class LogUtil {
    static public void logDebug(String str) {
        if (str != null) {
            Log.d("app", str);
        }

    }

    static public void logError(String str) {
        if (str != null) {
            Log.e("app", str);
        }
    }

    static public void logMessage(String str) {
        if (str != null) {
            Log.i("app", str);
        }
    }
}
