package com.gohoc.library.util;


import android.util.Log;

import com.gohoc.library.AppLogic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Created by Administrator on 2015/12/10 0010.
 */
public class PingUtil {

    public static void ping() {
        final String url = "www.baidu.com";
        //判断是否有网络
        if (!NetworkUtil.isNetworkAvailable(AppLogic.getAppContext())) {
            return;
        }
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String result = "";
                    StringBuffer r = new StringBuffer();
                    String jieguo = "";
                    try {
                        Process p = Runtime.getRuntime().exec("ping -c 10 -w 100 " + url);
                        int status = p.waitFor();

                        if (status == 0) {
                            result = "success";
                        } else {
                            result = "failed";
                        }
                        if (status == 0) {
                            String lost = new String();
                            String delay = new String();
                            BufferedReader buf = new BufferedReader(new InputStreamReader(p.getInputStream()));
                            String str = new String();
                            //读出所有信息并显示
                            while ((str = buf.readLine()) != null) {
                                str = str + "\r\n";
                                r.append(str);
                            }
                            if (!r.equals("")) {
                                jieguo = r.toString();
                                String last = (jieguo.substring(jieguo.lastIndexOf("="), jieguo.length())).trim();
                                String[] lastS = last.split("/");
                                String ping = lastS[1].trim();
                                Log.i("ping", ping);
                                //这里ping的结果

                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}

