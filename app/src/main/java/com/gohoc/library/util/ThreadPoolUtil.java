package com.gohoc.library.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池创建设置类
 * Created by keke on 2016/1/13 0013.
 */

public class ThreadPoolUtil {

    private static ThreadPoolUtil mInstance = null;
    //线程池
    private static ExecutorService mExecutorService = null;
    //默认线程数量为1个
    private static final int DEAFULT_THREAD_COUNT=3;

    public static ThreadPoolUtil getInstance() {
        if (mInstance == null) {
            synchronized (ThreadPoolUtil.class) {
                if (mInstance == null) {
                    mInstance = new ThreadPoolUtil();
                }
            }
        }
        return mInstance;
    }
    /*创建线程池*/
    public ExecutorService getExecutorService() {
//        双重判断 提高同步效率
        if (mExecutorService == null) {

            synchronized (ThreadPoolUtil.class){
                mExecutorService = Executors.newFixedThreadPool(DEAFULT_THREAD_COUNT);
            }
        }
        return mExecutorService;
    }
}
