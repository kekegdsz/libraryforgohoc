package com.gohoc.library.view;

import android.widget.GridView;

import com.gohoc.library.R;
import com.gohoc.library.adapter.GlideAdapter;

import java.util.ArrayList;
import java.util.List;

public class ShowGlideActivity extends BaseActivity{
    private GridView gridView;
    private GlideAdapter adapter;
    List<String> glidelist =new ArrayList<>();

    @Override
    public void setView() {
        setContentView(R.layout.activity_show_glide);
    }

    @Override
    public void initView() {
        gridView =(GridView)findViewById(R.id.gridview_glide);
        for(int i=1;i<100;i++) {
            glidelist.add("http://xiaobiaoqing.gohoc.com/uploads/100/14472192046077.gif");
        }
        adapter = new GlideAdapter(this,R.layout.layout_imageview_item);
        gridView.setAdapter(adapter);
        adapter.setDataSources(glidelist);
        adapter.notifyDataSetChanged();

    }


}
