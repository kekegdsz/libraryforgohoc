package com.gohoc.library.view;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gohoc.library.ContantValue;
import com.gohoc.library.R;
import com.gohoc.library.listview.DeleteListViewMainActivity;
import com.gohoc.library.util.LogUtil;
import com.mob.tools.utils.UIHandler;

import com.umeng.socialize.bean.LIKESTATUS;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.RequestType;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners;
import com.umeng.socialize.controller.listener.SocializeListeners.SocializeClientListener;
import com.umeng.socialize.view.ActionBarView;


import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;


public class MainActivity extends BaseActivity implements AdapterView.OnItemClickListener, PlatformActionListener, Handler.Callback {

    private String text = "这是我的分享测试数据！~我只是来酱油的！~请不要在意 好不好？？？？？";
    private String imageurl = "http://h.hiphotos.baidu.com/image/pic/item/ac4bd11373f082028dc9b2a749fbfbedaa641bca.jpg";
    private String title = "baidu";
    private String url = "www.baidu.com";
    private SharePopupWindow share;
    private TextView tvHint;
    private ListView listView;
    //各种框架ui显示的demoAdapter
    private ArrayAdapter<String> itemAdapter;

    @Override
    public void setView() {
        setContentView(R.layout.activity_splash);
        init();
    }

    private void init() {

        UMSocialService controller = UMServiceFactory.getUMSocialService(
                "com.umeng.like", RequestType.SOCIAL);
        controller.getEntity().getLikeStatus();
        controller.likeChange(this, new SocializeClientListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onComplete(int status, SocializeEntity entity) {
                if (entity != null) {
                    // 获取当前的“喜欢”状态
                    LIKESTATUS likeStatus = entity.getLikeStatus();
                }
            }

        });
        //用于集成ActionBar 的ViewGroup ActionBar 将填充提供的ViewGroup(需要开发者自己在布局文件或代码中创建
//，建议使用RelativeLayout)
        RelativeLayout parent = (RelativeLayout) findViewById(R.id.RelativeLayout);
//创建ActionBar des参数是ActionBar的唯一标识，请确保不为空
        ActionBarView socializeActionBar = new ActionBarView(this, "110");

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.FILL_PARENT);
        socializeActionBar.setLayoutParams(layoutParams);
//添加ActionBar
        parent.addView(socializeActionBar);

    }

    @Override
    public void initView() {

        listView = (ListView) findViewById(R.id.listView);
        tvHint = (TextView) findViewById(R.id.tv_hint);
        itemAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, ContantValue.mainItem);
        listView.setAdapter(itemAdapter);
        listView.setOnItemClickListener(this);
        Animation ani = new AlphaAnimation(0f, 1f);
        ani.setDuration(1500);
        ani.setRepeatMode(Animation.REVERSE);
        ani.setRepeatCount(Animation.INFINITE);
        tvHint.startAnimation(ani);
    }


    /**
     * 切换Activity
     *
     * @param class1
     */
    public void startIntent(Class class1) {
        Intent intent = new Intent(MainActivity.this, class1);
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtil.logMessage(String.valueOf(position));
        //各种演示效果
        Intent intent = null;
        switch (position) {
            //iosDialog高仿效果
            case 0:
                startIntent(ShowIosDialogActivity.class);
                overridePendingTransition(R.anim.in_translate_top,
                        R.anim.out_translate_top);
                break;
            //glide
            case 1:
                startIntent(ShowGlideActivity.class);
                overridePendingTransition(R.anim.in_translate_top,
                        R.anim.out_translate_top);
                break;
            //listview 侧滑删除
            case 2:
                startIntent(DeleteListViewMainActivity.class);
                overridePendingTransition(R.anim.in_translate_top,
                        R.anim.out_translate_top);
                break;
            case 3:
                startIntent(WelcomeActivity.class);
                overridePendingTransition(R.anim.in_translate_top,
                        R.anim.out_translate_top);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (share != null) {
            share.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShareSDK.stopSDK(this);
    }

    @Override
    public void onCancel(Platform arg0, int arg1) {
        Message msg = new Message();
        msg.what = 0;
        UIHandler.sendMessage(msg, this);
    }

    @Override
    public void onComplete(Platform plat, int action,
                           HashMap<String, Object> res) {
        Message msg = new Message();
        msg.arg1 = 1;
        msg.arg2 = action;
        msg.obj = plat;
        UIHandler.sendMessage(msg, this);
    }

    @Override
    public void onError(Platform arg0, int arg1, Throwable arg2) {
        Message msg = new Message();
        msg.what = 1;
        UIHandler.sendMessage(msg, this);
    }

    @Override
    public boolean handleMessage(Message msg) {
        int what = msg.what;
        if (what == 1) {
            Toast.makeText(this, "分享失败", Toast.LENGTH_SHORT).show();
        }
        if (share != null) {
            share.dismiss();
        }
        return false;
    }

    private void showShare() {
        ShareSDK.initSDK(this);
        share = new SharePopupWindow(this);
        share.setPlatformActionListener(this);
        ShareModel model = new ShareModel();
        model.setImageUrl(imageurl);
        model.setText(text);
        model.setTitle(title);
        model.setUrl(url);
        share.initShareParams(model);
        share.showShareWindow();
        // 显示窗口 (设置layout在PopupWindow中显示的位置)
        share.showAtLocation(this.findViewById(R.id.mainFrame),
                Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionsmenu, menu);
        android.view.SubMenu subMenu1 = menu.addSubMenu(0, 0, 104, "Action Item");
        subMenu1.add("光合科技");
        subMenu1.add("切换输入");
        subMenu1.add("帮助/关于");
        subMenu1.add("退出");
        MenuItem subMenu1Item = subMenu1.getItem();
        subMenu1Item.setIcon(R.drawable.menu);
        subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menu_about:
//                //从中间从小变大
//                overridePendingTransition(R.anim.small_2_big, R.anim.fade_out);
//                return true;
            case R.id.menu_feedback:
                //抽屉形式打开Activity从右到左
                UMSocialService controller = UMServiceFactory.getUMSocialService("com.umeng.comment",RequestType.SOCIAL);
                //是否强制登录后才能发表评论. 取值为false表示将以游客身份发表评论
                controller.openComment(this,false);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;
            case R.id.menu_share:
                showShare();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
