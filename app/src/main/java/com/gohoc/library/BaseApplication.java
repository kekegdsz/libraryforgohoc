package com.gohoc.library;

import android.app.Application;
/**
 * Created by Administrator on 2016/1/13 0013.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppLogic.setAppContext(getApplicationContext());

    }
}
