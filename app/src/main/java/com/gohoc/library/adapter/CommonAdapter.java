package com.gohoc.library.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keke on 2015/12/24.
 */
public abstract class CommonAdapter<T> extends BaseAdapter{

    protected  Context mContext;
    protected List<T> mDataSources =new ArrayList<T>();
    protected int mLayoutId;
    public CommonAdapter(Context context,int layoutId) {
        this.mContext=context;
        this.mLayoutId=layoutId;
    }
    public void setDataSources(List<T>dataSources){
        this.mDataSources =new ArrayList<T>(dataSources);
    }
    @Override
    public int getCount() {
        if(mDataSources.size()==0){
            return 0;
        }
        return mDataSources.size();
    }

    @Override
    public T getItem(int position) {
        return mDataSources.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = ViewHolder.get(mContext, convertView, parent,
                mLayoutId, position);
        convert(holder, getItem(position));
        return holder.getConvertView();
    }

    public abstract void convert(ViewHolder holder, T t);
}
