package com.gohoc.library.adapter;

import android.content.Context;

import com.gohoc.library.R;

import java.util.List;

/**
 * Created by Administrator on 2016/1/15 0015.
 */
public class GlideAdapter extends CommonAdapter<String> {
    public GlideAdapter(Context context, int layoutId) {
        super(context, layoutId);
    }

    @Override
    public void setDataSources(List<String> dataSources) {
        super.setDataSources(dataSources);
    }

    @Override
    public void convert(ViewHolder holder, String s) {
        holder.setImageForGlide(R.id.imageView_item, s);

    }
}
