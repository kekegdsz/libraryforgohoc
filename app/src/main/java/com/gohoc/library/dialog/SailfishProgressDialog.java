package com.gohoc.library.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.gohoc.library.R;
import com.gohoc.library.util.LogUtil;

/**
 * Created by ryan on 14-7-10.
 */
public class SailfishProgressDialog {
    private Context context;
    private Dialog dialog;
    private ImageView imageView;
    private TextView textView;
    private RotateAnimation rotateAnimation;

    public SailfishProgressDialog(Context context) {
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.layout_progress, null);

        imageView = (ImageView) rootView.findViewById(R.id.imageView);
        textView = (TextView) rootView.findViewById(R.id.textView);

        dialog = new Dialog(context, R.style.sailfishProgressDialog);
        dialog.setContentView(rootView);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                LogUtil.logMessage("dialog onDismiss");
            }
        });
        Window window = dialog.getWindow();
        window.setGravity(Gravity.TOP);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.y = 100;

        window.setAttributes(lp);
    }

    public RotateAnimation getRotateAnimation() {
        if (rotateAnimation == null) {
            rotateAnimation = new RotateAnimation(360, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

            rotateAnimation.setInterpolator(new LinearInterpolator());
            rotateAnimation.setRepeatCount(Animation.INFINITE);
            rotateAnimation.setDuration(600);
        }

        return rotateAnimation;
    }

    public SailfishProgressDialog showLoading(String text) {
        try {
            if (imageView != null) {
                imageView.setImageResource(R.drawable.dialog_load);
                imageView.clearAnimation();
                imageView.startAnimation(getRotateAnimation());
            }

            if (textView != null) {
                textView.setText(text);
            }
            show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public SailfishProgressDialog showFail(String text) {

        if (imageView != null) {
            imageView.setImageResource(R.drawable.dialog_fail);
            imageView.clearAnimation();
        }

        if (textView != null) {
            textView.setText(text);
        }
        show();
        return this;
    }

    public SailfishProgressDialog showSuccess(String text) {
        if (imageView != null) {
            imageView.setImageResource(R.drawable.dialog_sucess);
            imageView.clearAnimation();
        }

        if (textView != null) {
            textView.setText(text);
        }

        show();
        return this;
    }

    public SailfishProgressDialog showWarning(String text) {

        if (imageView != null) {
            imageView.setImageResource(R.drawable.dialog_warning);
            imageView.clearAnimation();
        }

        if (textView != null) {
            textView.setText(text);
        }
        show();
        return this;
    }

    public void dismiss() {
        try {
            if (dialog != null) {
                dialog.dismiss();
            }
        } catch (Throwable e) {

        }

    }

    public void delayDismiss(int ms) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();

            }
        }, ms);

    }

    public SailfishProgressDialog show() {
        try {
            if (dialog != null && !dialog.isShowing()) {
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return this;
    }

    static public SailfishProgressDialog build(Context context) {
        SailfishProgressDialog sailfishToast = new SailfishProgressDialog(context);

        return sailfishToast;
    }

}
